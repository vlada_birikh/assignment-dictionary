    section .text

%define EXIT_VALUE 60
%define BUFFER_VALUE 255
%define DEFOULT_VALUE 1
%define ERR_DESC 2
%define  U_VALUE 8
%include "colon.inc"
%include "lib.inc"
%define NEWLINE_VALUE 0xA
extern find_word
extern print_string
extern string_length                 
global _start


section .rodata
input_string_buffer: times BUFFER_VALUE db 0

s_msg: db "Please enter something ", NEWLINE_VALUE, 0
f_msg: db "Find this  ", NEWLINE_VALUE, 0
e_msg: db "The key length is more than 255",   NEWLINE_VALUE, 0     
n_msg:   db "The key isn't exist in dictionary", NEWLINE_VALUE, 0

%include "words.inc"

section .text
    _start:
		
		mov rdi, s_msg
        call print_string
		
        mov rdi, input_string_buffer                                    
        mov rsi, BUFFER_VALUE
        call read_word                                                 
        push rdx                                                      
        je .error
        mov rdi, rax                                                   
        mov rsi, CURRENT_START
        call find_word
        cmp rax, 0
        je .n_find
        add rax, U_VALUE                                             
        pop rdx
        add rax, rdx                                                   
        inc rax

		
        mov rdi, rax
        call print_string                                            
        mov rax, EXIT_VALUE
        syscall
    .error:
        jns error
	.n_find:
        jns n_find


error:
	mov rdi, e_msg
	call print_string_err
    mov rax, 1
	mov rax, EXIT_VALUE
    syscall
	
n_find:
	mov rdi, n_msg
	call print_string
	mov rax, EXIT_VALUE
    syscall


print_string_err:
    mov rsi, rdi
    call string_length                 
    mov rdx, rax                           
    mov rax, DEFOULT_VALUE                     
    mov rdi ERR_DESC                        
    syscall
    ret                         
    
