  %define EXIT_VALUE 60
%define DEFOULT_VALUE 1
%define NEWLINE_VALUE 10
%define WHITESPACE_VALUE 0x20
%define TABULATE_VALUE 0x9
%define CALCULUS_VALUE 10
%define ZERO_VALUE 0
%define MINUS_VALUE 0x2d
%define READ_VALUE 0

%define MASK 0x8000000000000000

%define CLEAR xor rax, rax

output:
    mov rax, DEFOULT_VALUE
    mov rdi, DEFOULT_VALUE
    syscall
    ret

exit:
    mov rax, EXIT_VALUE
    syscall
    ret

string_length:
    CLEAR
	.loop:
		cmp byte [rdi+rax], ZERO_VALUE
		je .end
		inc rax
		jmp .loop
	.end:
		ret

print_string:
    mov rsi, rdi
    push rdi
    push rsi

    call string_length
    pop rsi
    pop rdi
    mov rdx, rax
    push rax
    push rdi
    call output
    pop rdi
    pop rax
    ret


print_newline:

	mov rdi, NEWLINE_VALUE

print_char:
    push rdi
    mov rsi, rsp
    mov rdx, DEFOULT_VALUE
    push rax
    push rdi
    call output
    pop rdi
    pop rax
	pop rdi
    ret

print_int:
    mov r10, MASK
    mov rax, rdi
    and r10, rax
    test r10, r10
    jns print_uint
    push rax
    mov  rdi, MINUS_VALUE
    push rsi
    call print_char
    pop rsi
    pop rax
    neg rax
    mov rdi, rax


print_uint:
	push r12
    mov r12, rsp
    mov rax, rdi
    dec rsp
    mov byte[rsp], ZERO_VALUE
    .loop:
        xor rdx, rdx
        push r13
        mov r13, CALCULUS_VALUE
        div r13
        pop r13
        add rdx, '0'

        dec rsp
        mov  byte[rsp], dl
        test rax, rax
        jz .print
        jmp .loop
    .print:
        mov rdi, rsp
        push rdx
        call print_string
        pop rdx
        mov rsp, r12
        pop r12
        ret




string_equals:
	push rdi
    push rsi

    call string_length

    pop rdi
    pop rsi

    push rdi
    push rsi
    push rax

    call string_length

    pop rdx
    pop rsi
    pop rdi

    cmp rdx, rax
    jne .false
    xor rcx, rcx
.loop:
    mov dl, byte[rdi + rcx]
    cmp dl, byte[rsi + rcx]
    jne .false
    inc rcx
    cmp dl, ZERO_VALUE
    jne .loop
    mov rax, DEFOULT_VALUE
    ret
.false:
    CLEAR
    ret


read_char:
    mov rax, READ_VALUE
    mov rdi, READ_VALUE
    mov rdx, DEFOULT_VALUE
    push rax
    mov rsi, rsp
    syscall
    pop rax
    ret



read_word:
    xor rcx, rcx
    mov r8, rdi
    mov r9, rsi
    mov r10, rcx
    .loop:
        call read_char
        cmp al, WHITESPACE_VALUE
        je .sym
        cmp al, TABULATE_VALUE
        je .sym
        cmp al, NEWLINE_VALUE
        je .sym
        cmp rax, ZERO_VALUE
        je .return
        mov [r8 + r10], rax
        inc r10
        cmp r10, r9
        jge .over
        jmp .loop
    .over:
        xor rdx, rdx
        CLEAR
        ret
	.sym:
        cmp r10, ZERO_VALUE
        je .loop
    .return:
        mov byte [r8 + r10], ZERO_VALUE
        mov rax, r8
        mov rdx, r10
        ret


parse_uint:
    mov r8, CALCULUS_VALUE
    xor rcx, rcx
    xor rax, rax
.loop:
    movzx r9, byte[rdi + rcx]
    cmp r9b, ZERO_VALUE
    je .end
    cmp r9b, '0'
    jb .end
    cmp r9b, '9'
    ja .end
    mul r8
    sub r9b, '0'
    add rax, r9
    inc rcx
    jmp .loop
.end:
    mov rdx, rcx
    ret

parse_int:
	movzx r9, byte[rdi]
	cmp r9b, '-'
	je .neg
	push ZERO_VALUE
	jmp .par
.neg:
	push DEFOULT_VALUE
	inc rdi
.par:
    call parse_uint
    pop r9
    cmp r9, ZERO_VALUE
    je .pos
    neg rax
    inc rdx
.pos:
    ret

string_copy:
    push rdi
    push rsi
    call string_length
    pop rsi
    pop rdi
    inc rax
    cmp rax, rdx
    jg .small_buffer
    xor rcx, rcx
.loop:
    mov rdx, [rdi + rcx]
    mov [rsi + rcx], rdx
    inc rcx
    cmp byte[rdi + rcx], ZERO_VALUE
    jne .loop
    ret
.small_buffer:
    CLEAR
    ret
