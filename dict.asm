%include "lib.inc"
%define  U_VALUE 8
%define ZERO_VALUE 0
%define DEFOULT_VALUE 1
%define CLEAR xor rax, rax
global find_word

find_word:                                      
    push r12                                    
    .loop:
        cmp rsi, ZERO_VALUE                  
        je .not_found
        push rdi
        push rsi
        add rsi, U_VALUE                     
        call string_equals                      
        pop rsi
        pop rdi
        cmp rax, DEFOULT_VALUE                            
        je .found
    .next:
        mov r12, [rsi]                          
        mov rsi, r12                            
        jmp .loop                               
    .found:
        mov rax, rsi                           
        jmp .end
    .not_found:
        CLEAR
		jmp .end		
    .end:
        pop r12                              
        ret
