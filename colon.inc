
%define CURRENT_START 0
%macro colon 2                                   
    %ifid %2                                      
        %2:                                         
            dq CURRENT_START                        
            %define CURRENT_START %2                
         
    %endif

    %ifstr %1                                      
        db %1, 0                                   
    %endif
%endmacro
